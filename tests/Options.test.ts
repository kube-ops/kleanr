import { getBoolean, getNumber, getString, getStrings, getLogLevel } from '../src/Options'
import { LogLevel } from '../src/Logger'
import { expect } from 'chai'
import 'mocha'

describe('Parsing boolean value', function () {

    it('Empty value interpreted as "false" by default', function () {
        process.env.TESTVAR = ''
        let result = getBoolean('TESTVAR')
        expect(result).equal(false)
    })

    it('Empty value returns given fallback value', function () {
        process.env.TESTVAR = ''
        let withTrue = getBoolean('TESTVAR', true)
        let withFalse = getBoolean('TESTVAR', false)
        expect(withTrue).equal(true)
        expect(withFalse).equal(false)
    })

    it('Values "0", "no" and "false" converted to "false"', function () {
        for (let word of ['0', 'no', 'No', 'NO', 'false', 'False', 'FALSE']) {
            process.env.TESTVAR = word
            let result = getBoolean('TESTVAR', true)
            expect(result).equal(false)
        }
    })

    it('Any other non-empty values converted to "true"', function () {
        for (let word of ['-1', 'none', 'yes', '.', '-', '\n']) {
            process.env.TESTVAR = word
            let result = getBoolean('TESTVAR', false)
            expect(result).equal(true)
        }
    })

})

describe('Parsing numbers', function () {

    it('Empty value returns given fallback value', function () {
        process.env.TESTVAR = ''
        let result = getNumber('TESTVAR', 42)
        expect(result).equal(42)
    })

})

describe('Parsing strings', function () {

    it('Empty value returns given fallback value', function () {
        process.env.TESTVAR = ''
        let result = getString('TESTVAR', '42')
        expect(result).equal('42')
    })

    it('Empty value returns given fallback value', function () {
        const fallback: string[] = ['42']
        process.env.TESTVAR = ''
        let result = getStrings('TESTVAR', fallback)
        expect(result).members(fallback)
    })

    it('Spaces are ignored', function () {
        const expected: string[] = ['a', 'b', 'c', 'd', 'e', 'f']
        let result: string[] = new Array()

        process.env.TESTVAR = 'a b c d e f'
        result = getStrings('TESTVAR')
        expect(result).members(expected)

        process.env.TESTVAR = '     a     b     c      d  e     f    '
        result = getStrings('TESTVAR')
        expect(result).members(expected)

        process.env.TESTVAR = 'a    b   c      d  e         f'
        result = getStrings('TESTVAR')
        expect(result).members(expected)
    })

})

describe('Parsing log level value', function () {

    it('Empty value returns given fallback value', function () {
        process.env.TESTVAR = ''
        let result = getLogLevel('TESTVAR', LogLevel.Warn)
        expect(result).equal(LogLevel.Warn)
    })

    it('Level names "warning" and "warn" converted into the same log level value', function () {
        for (let value of ['warn', 'Warn', 'warning', 'warninG']) {
            process.env.TESTVAR = value
            let result = getLogLevel('TESTVAR')
            expect(result).equal(LogLevel.Warn)
        }
    })

})
