FROM node:15-alpine AS base

RUN set -eux \
  ; adduser -h /home/app -g Application -s /bin/sh -D -u 1042 app \
  ; mkdir -p /app /run/1042 \
  ; chown 1042:1042 /app /run/1042

WORKDIR /app
USER 1042:1042

# --- Build code

FROM base AS code-builder

USER root

RUN yarn global add typescript

COPY --chown=1042:1042 package.json package.json
COPY --chown=1042:1042 yarn.lock yarn.lock

USER 1042:1042

RUN yarn --frozen-lockfile --ignore-scripts

COPY --chown=1042:1042 tsconfig.json tsconfig.json
COPY --chown=1042:1042 src src

RUN tsc

# --- Build dependencies

FROM base AS deps-builder

COPY --chown=1042:1042 package.json package.json
COPY --chown=1042:1042 yarn.lock yarn.lock
COPY --chown=1042:1042 .yarnclean .yarnclean

ENV NODE_ENV production

RUN set -eux \
  ; yarn --production --pure-lockfile --frozen-lockfile --ignore-scripts \
  ; yarn autoclean --force

# --- Artifacts

FROM scratch AS build-artifacts

COPY --from=code-builder /app/dist /app/dist
COPY --from=code-builder /app/package.json /app/package.json
COPY --from=deps-builder /app/node_modules /app/node_modules

# --- Runtime

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

FROM node:15-alpine

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="kleanr" \
  org.label-schema.description="Kubernetes tool for keeping cluster clean" \
  org.label-schema.url="https://gitlab.com/kube-ops/kleanr" \
  org.label-schema.vcs-ref="${VCS_REF}" \
  org.label-schema.version="${VERSION}" \
  org.label-schema.vcs-url="https://gitlab.com/kube-ops/kleanr" \
  org.label-schema.vendor="Anton Kulikov" \
  org.label-schema.schema-version="1.0"

COPY --from=build-artifacts /app /app

USER 1042:1042
EXPOSE 9000-9001
WORKDIR /app
VOLUME [ "/run/1042" ]

CMD [ "node", "/app/dist/index.js" ]
