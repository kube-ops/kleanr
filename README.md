# Kleanr

Automatically deletes failed pods in cluster

## Deploy

Using [Helm](https://helm.sh) [chart](https://gitlab.com/kube-ops/helm/apps/kleanr)

```shell
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade kleanr kube-ops/kleanr --install --namespace kube-ops --create-namespace --wait
```

With [kubectl/kustomize](https://kubectl.docs.kubernetes.io/)

```shell
$ kubectl apply -k ./deploy/
# ...or...
$ kustomize build ./deploy/ | kubectl apply -f -
```

## Settings

Environment variables:

- `DRY_RUN` - Enables dry run mode. Default: `false`.
- `LOG_LEVEL` - Configures minimal logging level. Default: `info`.
- `ANNOTATION` - All pods annotated with this annotation will be ignored. Default: `kleanr.kube-ops.io/skip`.
- `SKIP_NAMESPACES` - Space-delimited namespaces list that will be ignored. Default: `kube-system`.
- `SKIP_ANNOTATED_PODS` - If `true`, all pods annotated with `ANNOTATION` will be ignored. Default: `true`.
- `MAX_PENDING_SECONDS` - Max lifetime of the pending pods in seconds. Default: `86400` (1 day).
