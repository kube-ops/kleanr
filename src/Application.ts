import { Logger } from './Logger'
import { Options } from './Options'
import * as k8s from './Kubernetes'

export class Application {
    constructor (
        private options: Options = new Options(),
        private log: Logger = new Logger(options.logLevel),
    ) {
        k8s.setLogLevel(log.logLevel)

        for (let signal of ['SIGINT', 'SIGTERM']) {
            process.on(signal, () => this.stop())
        }
    }

    async podUpdated(pod: k8s.Pod, isNew: boolean) {
        const { log } = this
        const { metadata, status } = pod
        const { name, namespace, annotations } = metadata as k8s.Metadata
        const { phase, conditions, startTime } = status as k8s.PodStatus
        const { annotation, dryRun, skipAnnotatedPods, maxPendingSeconds } = this.options

        if (!name || !namespace) return

        log.debug(`Pod ${namespace}/${name} is ${isNew ? 'created' : 'updated'}`)
        log.debug(`Pod ${namespace}/${name} is in phase ${phase} with conditions`, conditions)

        if (annotations && annotations[annotation] && skipAnnotatedPods) {
            log.info(`Skip annotated pod ${namespace}/${name}`)
            return
        }

        if (phase === 'Running') {
            log.info(`Skip running pod ${namespace}/${name}`)
            return
        }

        if (phase === 'Succeeded') {
            log.info(`Skip succeeded pod ${namespace}/${name}`)
            return
        }

        if (startTime) {
            const startedAt = new Date(startTime)
            const now = new Date()
            const seconds = (now.getTime() - startedAt.getTime()) / 1000

            if (seconds <= maxPendingSeconds) {
                log.info(`Skip pending pod ${namespace}/${name} started at`, startTime)
                return
            }
        }

        if (phase === 'Pending' && !startTime) {
            log.info(`Skip pending pod ${namespace}/${name} with unknown start time`)
            return
        }

        await k8s.deletePod(name, namespace, dryRun)
    }

    async start() {
        const podCallback = async (pod, isNew) => this.podUpdated(pod, isNew)
        k8s.watchNamespacedPods(this.options.skipNamespaces, podCallback)
        this.log.info(`Started`)
    }

    stop() {
        this.log.info(`Stopping`)
        process.exit()
    }
}
