import { LogLevel } from './Logger'

export function getBoolean(key: string, fallback: boolean = false): boolean {
    if (!process.env[key] || process.env[key] === '') return fallback

    const v = (process.env[key] || '').trim().toUpperCase()
    if (v === '0' || v === 'FALSE' || v === 'NO') return false
    return true
}

export function getNumber(key: string, fallback: number = 0): number {
    if (!process.env[key] || process.env[key] === '') return fallback
    return parseInt(process.env[key]!, 10)
}

export function getString(key: string, fallback: string = ''): string {
    if (!process.env[key] || process.env[key] === '') return fallback
    return process.env[key]!.trim()
}

export function getStrings(key: string, fallback: string[] = []): string[] {
    if (!process.env[key] || process.env[key] === '') return fallback
    return process.env[key]!.trim().split(' ').filter(v => v.trim() !== '')
}

export function getLogLevel(key: string, fallback: LogLevel = LogLevel.Info): LogLevel {
    const v = (process.env[key] || '').trim().toUpperCase()

    switch (v) {
        case 'DEBUG': return LogLevel.Debug
        case 'INFO': return LogLevel.Info
        case 'WARN': return LogLevel.Warn
        case 'WARNING': return LogLevel.Warn
        case 'ERROR': return LogLevel.Error

        default: return fallback
    }
}

export class Options {
    constructor (
        public dryRun: boolean = getBoolean('DRY_RUN'),
        public logLevel: LogLevel = getLogLevel('LOG_LEVEL', LogLevel.Info),
        public skipNamespaces: string[] = getStrings('SKIP_NAMESPACES', ['kube-system']),
        public skipAnnotatedPods: boolean = getBoolean('SKIP_ANNOTATED_PODS', true),
        public annotation: string = getString('SKIP_NAMESPACES', 'kleanr.kube-ops.io/skip'),
        public maxPendingSeconds: number = getNumber('MAX_PENDING_SECONDS', 86400),
    ) {}
}

export default new Options()
