export enum LogLevel {
    Error, ERROR = Error,
    Warn, WARN = Warn, Warning = Warn,
    Info, INFO = Info,
    Notice, NOTICE = Notice,
    Debug, DEBUG = Debug,
}

export class Logger {
    constructor(
        public logLevel: LogLevel = LogLevel.Info,
    ) {}

    debug(...args: any[]) {
        if (this.logLevel < LogLevel.Debug) return
        console.debug(...args)
    }

    info(...args: any[]) {
        if (this.logLevel < LogLevel.Info) return
        console.info(...args)
    }

    warn(...args: any[]) {
        if (this.logLevel < LogLevel.Warn) return
        console.warn(...args)
    }

    error(...args: any[]) {
        console.error(...args)
    }
}

export default new Logger()
