import * as k8s from '@kubernetes/client-node'
import log, { LogLevel } from './Logger'

const config = new k8s.KubeConfig()
config.loadFromDefault()

export const api = config.makeApiClient(k8s.CoreV1Api)

const watchNamespaces = k8s.makeInformer(config, `/api/v1/namespaces`, async () => api.listNamespace())
let watchPods: k8s.Informer<k8s.V1Pod>[] = []

export type Namespace = k8s.V1Namespace
export type Pod = k8s.V1Pod
export type PodStatus = k8s.V1PodStatus
export type Metadata = k8s.V1ObjectMeta

export type PodCallback = (pod: Pod, isNew: boolean) => void

async function restartInformer(informer: any) {
    await informer.start()
    log.info('Informer started')
}

function informerFailed(informer: any, err: any) {
    log.warn('Informer failed', err)
    setTimeout(async () => restartInformer(informer), 5000)
}

function namespaceUpdated(namespace: Namespace, skipNamespaces: string[], fn: PodCallback) {
    const { name } = namespace.metadata as Metadata
    const path = `/api/v1/namespaces/${name}/pods`

    if (!name) return

    if (watchPods[name] && skipNamespaces.includes(name)) return delete watchPods[name]
    if (watchPods[name] || skipNamespaces.includes(name)) return

    const listPods = () => api.listNamespacedPod(name)
    watchPods[name] = k8s.makeInformer(config, path, listPods)
    watchPods[name].on('add', pod => fn(pod, true))
    watchPods[name].on('update', pod => fn(pod, false))
    watchPods[name].start()

    log.info(`Watching pods in namespace ${name}`)
}

export async function deletePod(name, namespace, dryRun = false) {
    try {
        await api.deleteNamespacedPod(name, namespace, undefined, dryRun ? 'All' : undefined)
        log.info(`Pod ${namespace}/${name} deleted`)
    } catch (err) {
        const { statusCode, statusMessage } = err
        if (statusCode !== 404) {
            log.error(`Failed to delete pod ${namespace}/${name}`, { statusCode, statusMessage })
        }
    }
}

export function watchNamespacedPods(skipNamespaces: string[], fn: PodCallback) {
    watchNamespaces.on('add', async namespace => namespaceUpdated(namespace, skipNamespaces, fn))
    watchNamespaces.on('update', async namespace => namespaceUpdated(namespace, skipNamespaces, fn))
    watchNamespaces.on('delete', async namespace => delete watchPods[namespace.metadata!.name!])
    watchNamespaces.on('error', async err => informerFailed(watchNamespaces, err))

    watchNamespaces.start()

    log.info('Watching namespaces')
}

export function setLogLevel(level: LogLevel) {
    log.logLevel = level
}
